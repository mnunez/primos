# MARTA NÚÑEZ GARCÍA

# Se ha realizado un programa que solicita al usuario un número entero no negativo y como respuesta escribe la lista
# de los números primos menores o iguales que él separados por espacios.

numero = int(input("Dame un número entero no negativo: ")) # Solicitud de un número entero al usuario.
cont = 0 # Variable "cont" que aumenta cuando se indica que el número es primo (a través de un booleano).
if numero >= 0: # Condición número no negativo.
    print("Números primos iguales o menores: ")
    for n in range(1, numero + 1): # Bucle FOR que itera cada elemento en el rango indicado.
        es_primo = True # Booleano. Indicación de que el número es primo. Se indica inicialmente valor "True".
        for i in range(2, n): # Bucle FOR ----> divisores
            if n == i:
                break
            elif n % i == 0:
                es_primo = False # Indicación números no primos.
            else:
                continue
        if es_primo == True: # Indicación números primos.
            cont += 1 # Aumento variable "cont" cuando el número es primo.
            print(' ', n, end=' ') # Se muestra por pantalla cada uno de los números dentro del rango que son
                                   # primos.
else:
    print("ERROR. El número introducido debe ser ENTERO y NO NEGATIVO.") # Si el número introducido por el usuario
                                                                         # es negativo, se muestra un mensaje por
                                                                         # pantalla.











